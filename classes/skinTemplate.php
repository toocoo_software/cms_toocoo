<?php
class skinTemplate {

	/**
    * @var PDO The connection to the database
    */
	private $db;
	private $campaign;
	private $cwd;
	
	/**
    * Construct.
    * @param PDO $db_conn The database connection
    */
	
	public function __construct($dbConn, $Rcwd){
		$this->db = $dbConn;	
		$this->cwd = $Rcwd;
	}

	public function create_page($campaign, $header, $title, $simage, $anchor1, $link1, $anchor2, $link2, $offerimg1, $offerimg2, $gacode, $exit1, $exit2, $templateID, $fimage, $simage2, $mimage, $cimage, $revision) {
		
		
		//match up campaign title with campaigns from database
		$sql = "SELECT * from `T1_pages` WHERE `id` = :page_id";
		$q = $this->db->prepare($sql);
		$q->bindValue(':page_id', $page_id );
		$q->execute();
		
		
		//page creator var
		
		$sql = "INSERT INTO T1_pages (campaign, header, title, simage, anchor1, link1, anchor2, link2, offerimg1, offerimg2, gacode, exit1, exit2, templateID, fimage, simage2, mimage, cimage, revision) VALUES (:campaign, :header, :title, :simage, :anchor1, :link1, :anchor2, :link2, :offerimg1, :offerimg2, :gacode, :exit1, :exit2, :templateID, :fimage, :simage2, :mimage, :cimage, :revision);";
		$q = $this->db->prepare($sql);
		$q->bindValue(':campaign', $campaign );
		$q->bindValue(':header', $header);
		$q->bindValue(':title', $title );
		$q->bindValue(':simage', $simage);
		$q->bindValue(':anchor1', $anchor1);
		$q->bindValue(':link1', $link1);
		$q->bindValue(':anchor2', $anchor2);
		$q->bindValue(':link2', $link2);
		$q->bindValue(':offerimg1', $offerimg1);
		$q->bindValue(':offerimg2', $offerimg2);
		$q->bindValue(':gacode', $gacode);
		$q->bindValue(':exit1', $exit1);
		$q->bindValue(':exit2', $exit2);
		$q->bindValue(':templateID', $templateID);
		$q->bindValue(':fimage', $fimage);
		$q->bindValue(':simage2', $simage2);
		$q->bindValue(':mimage', $mimage);
		$q->bindValue(':cimage', $cimage);
		$q->bindValue(':revision', $revision);

		$q->execute();
		
		
		//we need to return the page id from the database to keep track of it
		//inside the page editor
		$sql="SELECT id FROM T1_pages ORDER BY id DESC LIMIT 1";
		$q = $this->db->prepare($sql);
		$q->execute();
		$page_id = $q->fetchColumn();
		$_SESSION["pageid"] = $page_id ;
		
		$this->write_page($page_id);
		
	}
	
	public function write_page($page_id)
	{
				
		//match up the session page id with database page id
		$sql = "SELECT * from `T1_pages` WHERE `id` = :page_id";
		$q = $this->db->prepare($sql);
		$q->bindValue(':page_id', $page_id );
		$q->execute();
		
		
		//Array with all elements, just as a reference for now
		$all_elements = array('pageid', 'campaign', 'header', 'title', 'body', 'simage', 'anchor1', 'link1', 'anchor2', 'link2', 'offerimg1', 'offerimg2', 'gacode', 'exit1', 'exit2', 'templateID', 'fimage', 'simage2', 'mimage', 'cimage', 'revision');
		
		//fetch the value that we want to write to the file
		for ($i=2; $i<sizeof($all_elements);$i++)
		{
			$q->execute();
			$all_elements[$i] = $q->fetchColumn($i);	
			$q->execute();
			$campaign = $q->fetchColumn(1);
			
			//replace elements on index page
			$filename = $this->cwd . "/" . $campaign . "/index.php";
			$data = file( $filename , FILE_IGNORE_NEW_LINES );
			$file_contents = str_replace('element'.$i.'Num', $all_elements[$i],$data, $count=1);
			file_put_contents($filename, implode('', $file_contents));
			
			//replace elements on exit page
			$filename = $this->cwd . "/" . $campaign . "/exit.php";
			$data = file( $filename , FILE_IGNORE_NEW_LINES );
			$file_contents = str_replace('element'.$i.'Num', $all_elements[$i],$data, $count=1);
			file_put_contents($filename, implode('', $file_contents));
			
			//replace gacode element
			//replace elements on exit page
			$filename = $this->cwd . "/" . $campaign . "/js/gacode.js";
			$data = file( $filename , FILE_IGNORE_NEW_LINES );
			$file_contents = str_replace('element'.$i.'Num', $all_elements[$i],$data, $count=1);
			file_put_contents($filename, implode('', $file_contents));
			
		}
		
	}

	//return an array of all the values of the database
	public function display_value($campaign){
	
		//grab all the db info related that campaign name
		//like (header, headline, offer links, etc...)
		
		$sql = "SELECT * from `T1_pages` WHERE `campaign` = :campaign";
		$q = $this->db->prepare($sql);
		$q->bindValue(':campaign', $campaign );
		$q->execute();
	
		//count number of columns
		
		$colcount = $q->columnCount();
		
		for($i=0; $i<$colcount; $i++){
			$q->execute();
			$result[$i] = $q->fetchColumn($i);
		}
		
		return $result;
	}
	
	
	//This function Lists the current created pages for proliferation for specific templates
	public function displayAvailPages($current_campaign, $templateID){
			
		//get total row count. This is to keep
		//track of how many pages that are created
		
		$sql2 = "SELECT COUNT(id) FROM T1_pages where `templateID` = :templateID";
		$q = $this->db->prepare($sql2);
		$q->bindValue(':templateID', $templateID);
		$q->execute();
		$rowCount = $q->fetchColumn(0);
	
		//Display all the page campaign names
		//from the database
		$sql = "SELECT campaign FROM T1_pages WHERE `campaign` != :current_campaign AND `templateID` = :templateID";
		$q = $this->db->prepare($sql);
		$q->bindValue(':current_campaign', $current_campaign);
		$q->bindValue(':templateID', $templateID);
		$q->execute();
	
		
		$rows = $q->fetchAll(PDO::FETCH_ASSOC);
		$i=0;
		foreach ($rows as $row) {
			$result[$i] = $row['campaign'];
			$i++;
		}
	
		for ($j=0; $j < $rowCount-1; $j++){
			echo '<input type="checkbox" name="'.$result[$j].'" id="'.$result[$j].'" value=1 >'.$result[$j].'';
			echo '</br>';
		}
	
	}
	
	//this contains all campaigns that can be selected (to exclude current campaign)
	public function listCampaigns($current_campaign){
			
		//get total row count. This is to keep
		//track of how many pages that are created
		
		$sql2 = "SELECT COUNT(id) FROM T1_pages";
		$q = $this->db->prepare($sql2);
		$q->execute();
		$rowCount = $q->fetchColumn(0);
	
		//Display all the page campaign names
		//from the database
		$sql = "SELECT campaign FROM T1_pages WHERE `campaign` != :current_campaign";
		$q = $this->db->prepare($sql);
		$q->bindValue(':current_campaign', $current_campaign);
		$q->execute();
	

		
		$rows = $q->fetchAll(PDO::FETCH_ASSOC);
		$i=0;
		foreach ($rows as $row) {
			$result[$i] = $row['campaign'];
			$i++;
		}
		
		return $result;
	
	}
	
	//Check campaign name through database
	public function check_camp_name($campaign){
		//Check if campaign is available
		
		$sql = "SELECT COUNT(id) FROM T1_pages where `campaign` = :campaign";
		$q = $this->db->prepare($sql);
		$q->bindValue(':campaign', $campaign);
		$q->execute();
		$rowCount = $q->fetchColumn(0);

		if($rowCount > 0)
		{
			exit("Name Already Taken, Please Try Another Campaign Name");
		}
		
	}	
	
	
	public function edit_page($campaign, $element, $value, $templateID){

		$sql = "UPDATE T1_pages SET ".$element."=:value WHERE `campaign` = :campaign";
		$q = $this->db->prepare($sql);
		$q->bindValue(':value', $value );
		$q->bindValue(':campaign', $campaign);
		$q->execute();
		
		
		//delete this record and then create a new entry

		if($templateID == 1)
		{
			shell_exec('rm -rf ' . $campaign . '/index.php');
			shell_exec('rm -rf ' . $campaign . '/exit.php');

			shell_exec('cp canvas/index.php ' . $campaign . '/');
			shell_exec('cp canvas/exit.php ' . $campaign . '/');
		}
		
		if($templateID == 2)
		{
			shell_exec('rm -rf ' . $campaign . '/index.php');
			shell_exec('rm -rf ' . $campaign . '/exit.php');
			shell_exec('rm -rf ' . $campaign . '/js/gacode.js');

			shell_exec('cp canvas2/index.php ' . $campaign . '/');
			shell_exec('cp canvas2/exit.php ' . $campaign . '/');
			shell_exec('cp canvas2/js/gacode.js ' . $campaign . '/js');
			
		}
		
		//we need to return the page id from the database to keep track of it
		//inside the page editor
		$sql="SELECT id FROM T1_pages WHERE `campaign` = :campaign";
		$q = $this->db->prepare($sql);
		$q->bindValue(':campaign', $campaign);
		$q->execute();
		$page_id = $q->fetchColumn();
		$_SESSION["pageid"] = $page_id ;
		
		$this->write_page($page_id);
	}
	
	public function proliferate_pages($campaigns, $elements, $values, $templateID){
	
		//size of array
		//first loop for campaigns
		
		for($i=0;$i<count($campaigns);$i++)
		{
			//second loop for elements and its values
			for($j=0;$j<count($elements);$j++){

				$sql = "UPDATE T1_pages SET ".$elements[$j]."=:value WHERE `campaign` = :campaign";
				$q = $this->db->prepare($sql);
				$q->bindValue(':value', $values[$j] );
				$q->bindValue(':campaign', $campaigns[$i]);
				$q->execute();
				
				//delete this record and then create a new entry.
				
				if($templateID == 1)
					{
						shell_exec('rm -rf ' . $campaigns[$i] . '/index.php');
						shell_exec('rm -rf ' . $campaigns[$i] . '/exit.php');

						shell_exec('cp canvas/index.php ' . $campaigns[$i] . '/');
						shell_exec('cp canvas/exit.php ' . $campaigns[$i] . '/');
					}
					
				if($templateID == 2)
					{
						echo "I am in template 2 now";
						shell_exec('rm -rf ' . $campaigns[$i] . '/index.php');
						shell_exec('rm -rf ' . $campaigns[$i] . '/exit.php');
						shell_exec('rm -rf ' . $campaigns[$i] . '/js/gacode.js');

						shell_exec('cp canvas2/index.php ' . $campaigns[$i] . '/');
						shell_exec('cp canvas2/exit.php ' . $campaigns[$i] . '/');
						shell_exec('cp canvas2/js/gacode.js ' . $campaigns[$i] . '/js');
						
					}
				
				//we need to return the page id from the database to keep track of it
				//inside the page editor
				$sql="SELECT id FROM T1_pages WHERE `campaign` = :campaign";
				$q = $this->db->prepare($sql);
				$q->bindValue(':campaign', $campaigns[$i]);
				$q->execute();
				$page_id = $q->fetchColumn();
				
				$this->write_page($page_id);
			}
		}
	
	}
}