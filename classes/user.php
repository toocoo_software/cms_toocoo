<?php

class user{
	/**
    * @var PDO The connection to the database
    */
	private $db;
	private $cwd;
	
	/**
    * Construct.
    * @param PDO $db_conn The database connection
    */
	public function __construct($dbConn, $Rcwd){
		$this->db = $dbConn;	
		$this->cwd = $Rcwd;
	}
	
	public function prep_login($username, $password){
	
		//sanitize entries 
		$username =  htmlentities($username, ENT_QUOTES);
		$password =  htmlentities($password, ENT_QUOTES);
		
		// backend empty field check for older browsers
		if ($username == '' || $password == '')
		{
			echo "Please fill in username and password";
		}	
		
		else{
			$this->validate_login($username,$password);
		}
	}
	
	public function validate_login($username, $password) {
		
		
		$salt = '$2a$07$randomstuffforsalt$';
		$hashed_password = crypt($password, $salt);
		
		$sql = "SELECT * FROM users WHERE username = :username AND password = :password";
		
		$q = $this->db->prepare($sql);
		$q->bindValue(':username', $username );
		$q->bindValue(':password', $hashed_password );
		$q->execute();
	
		//See if entry exits in database
		if ( $q->rowCount() > 0 ) {
			//passed Login
			echo "password verified!";
			$_SESSION['username'] = $username;
			$_SESSION['loggedin'] = TRUE;
			header("Location: admin.php");
		}
		else {
			//failed Login
			
			echo "incorrect username/password";
		}
		
	}
	
	function checkLoginStatus()
	{
		if (isset($_SESSION['loggedin']))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function logout()
	{
		session_start();
		session_unset();
		header("Location: login.php");
	}
	
	function displayPagesT1(){
		
		$templateCount = 2;
		
		for ($k=1;$k<$templateCount+1;$k++){
		
			//Get Row count of all campaigns in that template
			$sql2 = "SELECT COUNT(id) FROM `T1_pages` where `templateID` = :templateID";
			$q = $this->db->prepare($sql2);
			$q->bindValue(':templateID', $k );
			$q->execute();
			
			$rowCount[$k] = $q->fetchColumn(0);
		
			//Display all the page campaign names
			//from the database
			$sql = "SELECT campaign FROM `T1_pages` where `templateID` = :templateID";
			$q = $this->db->prepare($sql);
			$q->bindValue(':templateID', $k );
			$q->execute();
			
		
			$rows = $q->fetchAll(PDO::FETCH_ASSOC);
			$i=0;
			foreach ($rows as $row) {
				$result[$k][$i] = $row['campaign'];
				$i++;
			}
		}
		
		return array($rowCount, $result);
		
	}
	
	function delete_page($campaign){
		//Delete from database functionality
		
		
		$sql = "DELETE FROM T1_pages WHERE `campaign` = :campaign";
		$q = $this->db->prepare($sql);
		$q->bindValue('campaign', $campaign );
		$q->execute();
		
		$folder = $this->cwd . "/" . $campaign;	
		
		return $folder;
	}
	
	function rrmdir($dir) { 
	   if (is_dir($dir)) { 
		 $objects = scandir($dir); 
		 foreach ($objects as $object) { 
		   if ($object != "." && $object != "..") { 
			 if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object); 
		   } 
		 } 
		 reset($objects); 
		 rmdir($dir); 
	   } 
	}
}
	