<?php
include('includes/init.php');
include_once('includes/utils.php');
session_start();

//check login status
if ($user->checkLoginStatus() == FALSE) {
	header("Location: login.php");
}


//Campaign Name, Create Directory
$campaign = $_POST['campaign'];
$skinTemplate->check_camp_name($campaign);

//need to sanitize this further because
//it is being put into a shell_exec command later on
$campaign = stripslashes($campaign); 
$campaign = str_replace('/','',$campaign);
$campaign = htmlentities($campaign);

$dir = ' ./' . $campaign;

$templateID = $_POST['templateID'];

shell_exec('mkdir' . $dir);

if ($templateID == 1){
	//create a new dir
	shell_exec('cp -r ./canvas/*' . $dir);

	//set your folder path
	$path = $campaign . '/index1_files/' ; 
	
	//These entries don't exist in Template 1
	//However, they must be declared.
	
	//featured image
	$fimage = 0;

	//second body image
	$simage2 = 0;

	//magazine image
	$mimage = 0;

	//celebrity image
	$cimage = 0;

	//revision number
	$revision = 0;
	
}
if ($templateID ==2){
	shell_exec('cp -r ./canvas2/*' . $dir);

	//set your folder path
	$path = $campaign . '/images/' ; 
	
	//variables exclusive only to Template 2

	//featured image
	$fimage = add_image('fimage' , $path);

	//second body image
	$simage2 = add_image('simage2' , $path);

	//magazine image
	$mimage = add_image('mimage' , $path);

	//celebrity image
	$cimage = add_image('cimage' , $path);

	//revision number
	$revision = $_POST['revision'];
}

//header Image
$header = add_image('header', $path);

//Headline 
$title = $_POST['title'];

//Side Image
$simage = add_image('simage' , $path);

//offer 1 anchor text 
$anchor1 = $_POST['anchor1'];

//offer 1 affiliate link
$link1 = $_POST['link1'];

//exit 1 link
$exit1 = $_POST['exit1'];

//offer 2 anchor text 
$anchor2 = $_POST['anchor2'];

//offer 2 affiliate link
$link2 = $_POST['link2'];

//exit 2 link
$exit2 = $_POST['exit2'];

//offer 1 Image
$offerimg1 = add_image('offerimg1' , $path);

//offer 2 Image
$offerimg2 = add_image('offerimg2' , $path);

//Google Analytics code
$gacode = $_POST['gacode'];


$skinTemplate->create_page($campaign, $header, $title, $simage, $anchor1, $link1, $anchor2, $link2, $offerimg1, $offerimg2, $gacode, $exit1, $exit2, $templateID, $fimage, $simage2, $mimage, $cimage, $revision);

header("Location: ".$campaign."");