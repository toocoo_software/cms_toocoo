<script type="text/javascript">

$(document).ready(function() {
	$('#Loading').hide(); 
	$("#error1").hide();
	$("#error2").hide();
	$("#error3").hide();
	$("#error4").hide();	
});

//validate campaign name for duplicate name
//using Ajax
function check_campaign_name(){

	var campaign = $("#campaign").val();
	if(campaign.length > 0){
		$('#Loading').show();
		$.get("check_campaign_name.php", {
			campaign: $('#campaign').val(),
		}, function(response){
			$('#Info').fadeOut();
			 $('#Loading').hide();
			setTimeout("finishAjax('Info', '"+escape(response)+"')", 450);
		
		var data = response;
		
		//disable submit button if campaign name already taken
		
		if (data.indexOf("data0") != -1){
				document.getElementById("create").disabled = true; 
		}
		
		else if (data.indexOf("data1") != -1){
				document.getElementById("create").disabled = false; 
		}
		
		});
		return false;           
	}
}

function finishAjax(id, response){
 
  $('#'+id).html(unescape(response));
  $('#'+id).fadeIn(1000);
} 

//validates image on frontend
//gives error when wrong img extension
function check_image(){
	var id_value = document.getElementById('header').value;
	var id_value2 = document.getElementById('simage').value;
	
  

	if(id_value != '' || id_value2 != '') 
	{ 
	  var valid_extensions = /(.jpg|.jpeg|.gif|.png)$/i; 
	  if(valid_extensions.test(id_value))
	  { 
		document.getElementById("create").disabled = false; 
	  }
	  else if(valid_extensions.test(id_value2)){
		document.getElementById("create").disabled = false;
	  }
	  else
	  {
		if (id_value !=''){
		$("#error1").show();
		}
		if (id_value2 !=''){
		$("#error2").show();
		}

		document.getElementById("create").disabled = true;   
	  }
	}
	 
}

	
</script>