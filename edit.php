<?php include( 'includes/init.php'); 
include( 'includes/utils.php'); 
include( 'ci_editor.php'); 
session_start(); 
//check login status 
if ($user->checkLoginStatus() == FALSE) 
{ 
	header("Location: login.php"); 
} 
//If user presses logout button 
if (isset($_POST['logout'])){ 
	$user->logout(); 
} 

$templateID = $_GET['templateID'];

//sanitize $templateID input

if(is_numeric($templateID) == FALSE)
{
	exit("templatedID is not correct");
}

if ($templateID == 1)
{
	$img_dir = $result[1] . '/index1_files/'; 
}

if ($templateID == 2)
{
	$img_dir = $result[1] . '/images/';
}
		
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    
    <head>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/bootstrap-fileupload.min.css">
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="js/bootstrap-fileupload.js"></script>
        <script type="text/javascript">
            $('form').submit(function() {
                if (!$('input[type="file"]').val()) {
                    // No file is uploaded, do not submit.
                    return false;
                } else {
                    return true;
                }
            });
		
	
			$(function() {
				$(":checkbox").click(function(){
					$.post("ci_editor.php", { id: this.id, checked: this.checked });
					var value = new Array();
					value = $(this).attr('id');
					console.log(value);
				});    
			});

            $(document).ready(function() {
                $('#Loading').hide();
                $("#error1").hide();
                $("#error2").hide();
				$("#error21").hide();
				$("#error22").hide();
				$("#error23").hide();
				$("#error24").hide();
                $("#error3").hide();
                $("#error4").hide();
                $("#errorURL1").hide();
				$("#errorURL11").hide();
                $("#errorURL2").hide();
				$("#errorURL21").hide();
				$("#proliferate").hide();

                //disable campaign input field and grey it out
                document.getElementById("campaign").disabled = true;
                document.getElementById("campaign").style.backgroundColor = "#CCCCCC";

				var templateID= <?php echo $_GET['templateID']; ?>;
		
				if (templateID == 1){
					$('.templateID2').hide();
					$('.templateID2').prop('required',false);
					$('.templateID2').change(function(){
						check_image();
					});
				}
		
            });

            var checker = 0;



            //validates image on frontend
            //gives error when wrong img extension
            function check_image(){
				var id_value = document.getElementById('header').value;
				var id_value2 = document.getElementById('simage').value;
				var id_value21 = document.getElementById('fimage').value;
				var id_value22 = document.getElementById('simage2').value;
				var id_value23 = document.getElementById('mimage').value;
				var id_value24 = document.getElementById('cimage').value;
				var id_value3 = document.getElementById('offerimg1').value;
				var id_value4 = document.getElementById('offerimg2').value;
		

				var valid_extensions = /(.jpg|.jpeg|.gif|.png)$/i; 

				if(id_value != '') 
				{ 
				  if(!valid_extensions.test(id_value))
				  { 
					$("#error1").show(); 
					document.getElementById("create").disabled = true; 
				  }
				  
				}
				
				
				if(id_value2 != '')
				{
					if(!valid_extensions.test(id_value2)){
						$("#error2").show();
						document.getElementById("create").disabled = true;
					  }	
				}
				
				
				if(id_value21 != '')
				{
					if(!valid_extensions.test(id_value21)){
						$("#error21").show();
						document.getElementById("create").disabled = true;
					  }	
				}
				
				
				if(id_value22 != '')
				{
					if(!valid_extensions.test(id_value22)){
						$("#error22").show();
						document.getElementById("create").disabled = true;
					  }	
				}
				
				
				if(id_value23 != '')
				{
					if(!valid_extensions.test(id_value23)){
						$("#error23").show();
						document.getElementById("create").disabled = true;
					  }	
				}
				
				
				if(id_value24 != '')
				{
					if(!valid_extensions.test(id_value24)){
						$("#error24").show();
						document.getElementById("create").disabled = true;
					  }	
				}
				
				if(id_value3 != '')
				{
					if(!valid_extensions.test(id_value3)){
						$("#error3").show();
						document.getElementById("create").disabled = true;
					  }	
				}
				
				if(id_value4 != '')
				{
					if(!valid_extensions.test(id_value4)){
						$("#error4").show();
						document.getElementById("create").disabled = true;
					  }	
				}
				
				if(id_value != '' && id_value2 != '' && id_value21 != '' && id_value22 != '' && id_value23 != '' && id_value24 != '' && id_value3 != '' && id_value4 != '') 
				{
					if(valid_extensions.test(id_value) && valid_extensions.test(id_value2) && valid_extensions.test(id_value21) && valid_extensions.test(id_value22) && valid_extensions.test(id_value23) && valid_extensions.test(id_value24) && valid_extensions.test(id_value3) && valid_extensions.test(id_value4))
					{
						document.getElementById("create").disabled = false;
					
					}
				}
			}



            function ValidURL() {

                var URL1 = document.form.link1.value;
				var EXITURL1 = document.form.exit1.value;
				var URL2 = document.form.link2.value;
				var EXITURL2 = document.form.exit2.value;

				
				var pattern = /^http[s]*\:\/\/[wwW]{3}\.+[a-zA-Z0-9]+\.[a-zA-Z]{2,3}.*$|^http[s]*\:\/\/[^w]{3}[a-zA-Z0-9]+\.[a-zA-Z]{2,3}.*$|http[s]*\:\/\/[0-9]{2,3}\.[0-9]{2,3}\.[0-9]{2,3}\.[0-9]{2,3}.*$/;
				
				//first check input field is empty or not
				if($.trim($("#link1").val()) != ""){
				
					if(!pattern.test(URL1)) {
						$("#errorURL1").show();
						document.getElementById("create").disabled = true;
					}

					else if(pattern.test(URL1)) {
						$("#errorURL1").hide();   
					}
				}
				
				if($.trim($("#exit1").val()) != ""){
				
					if(!pattern.test(EXITURL1)) {
						$("#errorURL11").show();
						document.getElementById("create").disabled = true;
					}

					else if(pattern.test(EXITURL1)) {
						$("#errorURL11").hide();   
					}
				}
					
				if ($.trim($("#link2").val()) != ""){
				
					if(!pattern.test(URL2)){
						$("#errorURL2").show();
						document.getElementById("create").disabled = true;
					}

					else if(pattern.test(URL2)) {
						$("#errorURL2").hide();   
					}
				}
				
				if($.trim($("#exit2").val()) != ""){
				
					if(!pattern.test(EXITURL2)) {
						$("#errorURL21").show();
						document.getElementById("create").disabled = true;
					}

					else if(pattern.test(EXITURL2)) {
						$("#errorURL21").hide();   
					}
				}
				
				if ($.trim($("#link1").val()) != "" && $.trim($("#link2").val()) != "" && $.trim($("#exit1").val()) != "" && $.trim($("#exit2").val()) != "" ){
					if((pattern.test(URL1)) && (pattern.test(URL2)) && (pattern.test(EXITURL1)) && (pattern.test(EXITURL2))){
						document.getElementById("create").disabled = false;
						
					}
				}

			}
   
		</script>
    </head>
    
    <body>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Toocoo CMS</title>
        <section class="container2">
            <div class="admin">
                	<h1>Edit Page</h1>

                <!-- Logout button -->
                <form method="post" action="admin.php">
                    <p class="submit">
                        <input type="submit" name="logout" value="Logout">
                    </p>
                </form>
				
				<!-- Editing Form -->
                <form id="form" name="form" method="post" enctype="multipart/form-data" action="ci_editor.php?campaign=<?php echo $result[1];?>">
                    <p class="create">
                        <h2>Campaign Name:</h2>
						<input type="text" name="campaign" id="campaign" value="<?php echo $result[1];?>" />
						<div id="Info"></div>	<span id="Loading"><img src="img/loader2.gif" alt="" /></span>
                        <br>
                        <br>
                    </p>
                    	
					<h2> Header Image:</h2> 	
					<div id="error1">
						<h2> <font color="red">Error, Wrong Image Format!</font> </h2> 
					</div><span id="result1"></span>
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: auto; height: auto;">
							<img src="<?php echo $img_dir . $result[2];?>" />
						</div>
						<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;">
						</div>
						<div>	
						<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
							<input type="file" name="header" id="header" onchange="check_image();" />
							</span>	<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error1").hide();'>Remove</a>
						</div>
					</div>
					
                    <p class="create">
                        <h2> Title Headline: </h2>
                        <textarea class="ckeditor" name="title" id="title">
                            <?php echo $result[3]; ?>
                        </textarea>
                        <br>
                        <br>
                    </p>
					
					<!--Featured Image -->
					<div class="templateID2">
						<h2> Featured Image:</h2> 
						<div id="error21">
							<h2> <font color="red">Error, Wrong Image Format!</font> </h2> 
						</div>
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<div class="fileupload-new thumbnail" style="width: auto; height: auto;">
								<img src="<?php echo $img_dir . $result[16];?>" />
							</div>
							<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
							<div>	<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
								<input type="file" name="fimage" id="fimage" onchange="check_image();" />
								</span>	<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error21").hide();'>Remove</a>
							</div>
						</div>
						</br>
						</br>
					</div>
					
					<!--First Body Image -->
                    <h2> First Body Image:</h2> 
					<div id="error2">
						<h2> <font color="red">Error, Wrong Image Format!</font> </h2> 
					</div>
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: auto; height: auto;">
							<img src="<?php echo $img_dir . $result[4];?>" />
						</div>
						<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
						<div>	<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
							<input type="file" name="simage" id="simage" onchange="check_image();" />
							</span>	<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error2").hide();'>Remove</a>
						</div>
						</br>
						</br>
					</div>
					
					<!--Second Body Image -->
					<div class="templateID2">
						<h2> Second Body Image:</h2> 
						<div id="error22">
							<h2> <font color="red">Error, Wrong Image Format!</font> </h2> 
						</div>
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<div class="fileupload-new thumbnail" style="width: auto; height: auto;">
								<img src="<?php echo $img_dir . $result[17];?>" />
							</div>
							<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
							<div>	<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
								<input type="file" name="simage2" id="simage2" onchange="check_image();" />
								</span>	<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error22").hide();'>Remove</a>
							</div>
						</div>
						</br>
						</br>
					</div>
					
					<!--Magazine Image -->
					<div class="templateID2">
						<h2> Magazine Image:</h2> 
						<div id="error23">
							<h2> <font color="red">Error, Wrong Image Format!</font> </h2> 
						</div>
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<div class="fileupload-new thumbnail" style="width: auto; height: auto;">
								<img src="<?php echo $img_dir . $result[18];?>" />
							</div>
							<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
							<div>	<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
								<input type="file" name="mimage" id="mimage" onchange="check_image();" />
								</span>	<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error23").hide();'>Remove</a>
							</div>
						</div>
						</br>
						</br>
					</div>
					
					<!--Celebrity Image -->
					<div class="templateID2">
						<h2> Celebrity Image:</h2> 
						<div id="error24">
							<h2> <font color="red">Error, Wrong Image Format!</font> </h2> 
						</div>
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<div class="fileupload-new thumbnail" style="width: auto; height: auto;">
								<img src="<?php echo $img_dir . $result[19];?>" />
							</div>
							<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
							<div>	<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
								<input type="file" name="cimage" id="cimage" onchange="check_image();" />
								</span>	<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error24").hide();'>Remove</a>
							</div>
						</div>
						</br>
						</br>
					</div>
					
						
                    <p class="create">
                        	<h2> Offer 1 </h2>

                        <input type="text" name="anchor1" value="<?php echo $result[6];?>" />
                        <br>
                        <br>
                    </p>
                    <p class="create">
                        	<h2> Offer 1 Link</h2>
                        <div id="errorURL1">
                             <h2> <font color="red">Error, Please Insert a URL starting with http:// </font> </h2> 
                        </div>
                        <input type="text" name="link1" id="link1" value="<?php echo $result[7];?>" onblur="ValidURL();" />
                        <br>
                        <br>
                    </p>
					
					<p class="create">
                        	<h2> Offer 1 Exit Link</h2>
                        <div id="errorURL11">
                             <h2> <font color="red">Error, Please Insert a URL starting with http:// </font> </h2> 
                        </div>
                        <input type="text" name="exit1" id="exit1" value="<?php echo $result[13];?>" onblur="ValidURL();" />
                        <br>
                        <br>
                    </p>
					
                    <p class="create">
                        	<h2> Offer 2</h2>

                        <input type="text" name="anchor2" value="<?php echo $result[8];?>" />
                        <br>
                        <br>
                    </p>
                    <p class="create">
                        	<h2> Offer 2 Link</h2>
                        <div id="errorURL2">
                             <h2> <font color="red">Error, Please Insert a URL starting with http:// </font> </h2> 
                        </div>
                        <input type="text" name="link2" id="link2" value="<?php echo $result[9];?>" onblur="ValidURL();" />
                        <br>
                        <br>
                    </p>
					
					<p class="create">
                        	<h2> Offer 2 Exit Link</h2>
                        <div id="errorURL21">
                             <h2> <font color="red">Error, Please Insert a URL starting with http:// </font> </h2> 
                        </div>
                        <input type="text" name="exit2" id="exit2" value="<?php echo $result[14];?>" onblur="ValidURL();" />
                        <br>
                        <br>
                    </p>
					
					<!--offer image 1-->
                    <h2> Offer Image 1:</h2> 
                    <div id="error3">
                         <h2> <font color="red">Error, Wrong Image Format!</font> </h2> 
                    </div>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: auto; height: auto;">
                            <img src="<?php echo $img_dir . $result[10];?>" />
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
                        <div>	<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                            <input type="file" name="offerimg1" id="offerimg1" onchange="check_image();" />
                            </span>	<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error3").hide();'>Remove</a>

                        </div>
						<br/>
						<br/>
                    </div>
               				
					<!--offer image 1-->
                    	<h2> Offer Image 2:</h2> 
                    <div id="error4">
                         <h2> <font color="red">Error, Wrong Image Format!</font> </h2> 
                    </div>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: auto; height: auto;">
                            <img src="<?php echo $img_dir . $result[11];?>" />
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
                        <div>	<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                            <input type="file" name="offerimg2" id="offerimg2" onchange="check_image();" />
                            </span>	<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error4").hide();'>Remove</a>

                        </div>
						<br/>
						<br/>
                    </div>
					
					<div class="templateID2">
						<p class="create"> 
							<h2> Revision Number</h2>
							<input class="templateID2" type="text" name="revision" value="<?php echo $result[20];?>" required />
							<br>
							<br>
						</p>
					</div>
					
                    	<h2>Google Analytics Code</h2> 
                    <div id="errorTextArea">
                        <h2><font color="red>Error, This Field is Required"</font></h2>
                    </div>
                    <textarea name="gacode" id="gacode" rows="15" cols="50"><?php echo $result[12];?></textarea>
                    
					<br/>
                    <br/>
                    <br/>
					
					<p class="create">
						<h2 font color="black"> Would You Like to Proliferate The Changes You Made? </h2>
						<input type="radio" name="proliferate" id="yes" class="a" value="Yes" onclick='$("#proliferate").show();'>Yes</br>
						<input type="radio" name="proliferate" id="no" class="a" value="No" onclick='$("#proliferate").hide();'>No
					<div id="proliferate">
					</br>
					<?php $skinTemplate->displayAvailPages($result[1], $templateID); ?>
					</div>
					</p>
					</br>
					<input type="hidden" name="templateID" id="templateID" value="<?php echo $_GET['templateID']; ?>" >
					
                    <p class="create">
                        <input type="submit" name="edit" id="edit" value="Save">
                    </p>					
					
                </form>
        </section>
    </body>

</html>