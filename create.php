<?php
include('includes/init.php');
include('includes/utils.php');
session_start();

//check login status
if ($user->checkLoginStatus() == FALSE) {
	header("Location: login.php");
}
//If user presses logout button
if (isset($_POST['logout'])) {
	$user->logout();
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/bootstrap-fileupload.min.css">
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
<script src="js/bootstrap-fileupload.min.js"></script>

<script type="text/javascript">

	$('form').submit(function(){  
	   if(!$('input[type="file"]').val()) {
			// No file is uploaded, do not submit.
			//not implemented yet
			return false;
		}
		else {
			return true;
		}
	});

	$(document).ready(function() {
		$('#Loading').hide(); 
		$("#error1").hide();
		$("#error2").hide();
		$("#error21").hide();
		$("#error22").hide();
		$("#error23").hide();
		$("#error24").hide();
		$("#error3").hide();
		$("#error4").hide();	
		$("#errorURL1").hide();
		$("#errorURL11").hide();
		$("#errorURL2").hide();
		$("#errorURL21").hide();		
		
		var templateID= <?php echo $_POST['templateID']; ?>;
		
		if (templateID == 1){
			$('.templateID2').hide();
			$('.templateID2').prop('required',false);
			$('.templateID2').change(function(){
				check_image();
			});
		}
		
	});
	
	
	

	var checker = 0;
	
	//validate campaign name for duplicate name
	//using Ajax
	function check_campaign_name(){

		var campaign = $("#campaign").val();
		if(campaign.length > 0){
			$('#Loading').show();
			$.get("check_campaign_name.php", {
				campaign: $('#campaign').val(),
			}, function(response){
				$('#Info').fadeOut();
				 $('#Loading').hide();
				setTimeout("finishAjax('Info', '"+escape(response)+"')", 450);
			
			var data = response;
			
			//disable submit button if campaign name already taken
			
			if (data.indexOf("data0") != -1){
					document.getElementById("create").disabled = true; 
			}
			
			else if (data.indexOf("data1") != -1){
					document.getElementById("create").disabled = false; 
					
			}
			
			});
			return false;           
		}
	}

	function finishAjax(id, response){
	 
	  $('#'+id).html(unescape(response));
	  $('#'+id).fadeIn(1000);
	} 

	//validates image on frontend
	//gives error when wrong img extension
	function check_image(){
		var id_value = document.getElementById('header').value;
		var id_value2 = document.getElementById('simage').value;
		var id_value21 = document.getElementById('fimage').value;
		var id_value22 = document.getElementById('simage2').value;
		var id_value23 = document.getElementById('mimage').value;
		var id_value24 = document.getElementById('cimage').value;
		var id_value3 = document.getElementById('offerimg1').value;
		var id_value4 = document.getElementById('offerimg2').value;
		

		var valid_extensions = /(.jpg|.jpeg|.gif|.png)$/i; 

		if(id_value != '') 
		{ 
		  if(!valid_extensions.test(id_value))
		  { 
			$("#error1").show(); 
			document.getElementById("create").disabled = true; 
		  }
		  
		}
		
		
		if(id_value2 != '')
		{
			if(!valid_extensions.test(id_value2)){
				$("#error2").show();
				document.getElementById("create").disabled = true;
			  }	
		}
		
		
		if(id_value21 != '')
		{
			if(!valid_extensions.test(id_value21)){
				$("#error21").show();
				document.getElementById("create").disabled = true;
			  }	
		}
		
		
		if(id_value22 != '')
		{
			if(!valid_extensions.test(id_value22)){
				$("#error22").show();
				document.getElementById("create").disabled = true;
			  }	
		}
		
		
		if(id_value23 != '')
		{
			if(!valid_extensions.test(id_value23)){
				$("#error23").show();
				document.getElementById("create").disabled = true;
			  }	
		}
		
		
		if(id_value24 != '')
		{
			if(!valid_extensions.test(id_value24)){
				$("#error24").show();
				document.getElementById("create").disabled = true;
			  }	
		}
		
		if(id_value3 != '')
		{
			if(!valid_extensions.test(id_value3)){
				$("#error3").show();
				document.getElementById("create").disabled = true;
			  }	
		}
		
		if(id_value4 != '')
		{
			if(!valid_extensions.test(id_value4)){
				$("#error4").show();
				document.getElementById("create").disabled = true;
			  }	
		}
		
		if(id_value != '' && id_value2 != '' && id_value21 != '' && id_value22 != '' && id_value23 != '' && id_value24 != '' && id_value3 != '' && id_value4 != '') 
		{
			if(valid_extensions.test(id_value) && valid_extensions.test(id_value2) && valid_extensions.test(id_value21) && valid_extensions.test(id_value22) && valid_extensions.test(id_value23) && valid_extensions.test(id_value24) && valid_extensions.test(id_value3) && valid_extensions.test(id_value4))
			{
				document.getElementById("create").disabled = false;
			
			}
		}

	 
	}


	function ValidURL(){

		var URL1 = document.form.link1.value;
		var EXITURL1 = document.form.exit1.value;
		var URL2 = document.form.link2.value;
		var EXITURL2 = document.form.exit2.value;

		
		var pattern = /^http[s]*\:\/\/[wwW]{3}\.+[a-zA-Z0-9]+\.[a-zA-Z]{2,3}.*$|^http[s]*\:\/\/[^w]{3}[a-zA-Z0-9]+\.[a-zA-Z]{2,3}.*$|http[s]*\:\/\/[0-9]{2,3}\.[0-9]{2,3}\.[0-9]{2,3}\.[0-9]{2,3}.*$/;
		
		//first check input field is empty or not
		if($.trim($("#link1").val()) != ""){
		
			if(!pattern.test(URL1)) {
				$("#errorURL1").show();
				document.getElementById("create").disabled = true;
			}

			else if(pattern.test(URL1)) {
				$("#errorURL1").hide();   
			}
		}
		
		if($.trim($("#exit1").val()) != ""){
		
			if(!pattern.test(EXITURL1)) {
				$("#errorURL11").show();
				document.getElementById("create").disabled = true;
			}

			else if(pattern.test(EXITURL1)) {
				$("#errorURL11").hide();   
			}
		}
			
		if ($.trim($("#link2").val()) != ""){
		
			if(!pattern.test(URL2)){
				$("#errorURL2").show();
				document.getElementById("create").disabled = true;
			}

			else if(pattern.test(URL2)) {
				$("#errorURL2").hide();   
			}
		}
		
		if($.trim($("#exit2").val()) != ""){
		
			if(!pattern.test(EXITURL2)) {
				$("#errorURL21").show();
				document.getElementById("create").disabled = true;
			}

			else if(pattern.test(EXITURL2)) {
				$("#errorURL21").hide();   
			}
		}
		
		if ($.trim($("#link1").val()) != "" && $.trim($("#link2").val()) != "" && $.trim($("#exit1").val()) != "" && $.trim($("#exit2").val()) != "" ){
			if((pattern.test(URL1)) && (pattern.test(URL2)) && (pattern.test(EXITURL1)) && (pattern.test(EXITURL2))){
	        	document.getElementById("create").disabled = false;
				
			}
		}
		
	}	


</script>
 
</head>
<body>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Toocoo CMS</title>

<section class="container2">
	<div class="admin">
		<h1>Create New Page</h1>
		
		<!-- Logout button -->
		<form method="post" action="admin.php">
			
			<p class="submit"><input type="submit" name="logout" value="Logout"></p>
		</form>
		
		
		<form id="form" name="form" method="post" enctype="multipart/form-data" action="m_create.php" >
			
			<p class="create">
			<h2>Campaign Name:</h2>
			<input type="text" name="campaign" id="campaign" maxlength="10" required onblur="return check_campaign_name();" />
			<div id="Info"></div>
			<span id="Loading"><img src="img/loader2.gif" alt="" /></span>
			<br>
			<br>
			</p>
			

			
			<h2> Header Image:</h2> <div id="error1"> <h2> <font color="red">Error, Wrong Image Format!</font> </h2> </div>
			<span id="result1"></span>
			<div class="fileupload fileupload-new" data-provides="fileupload">
			  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
			  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
			  <div>
				<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="header" id="header" onchange="check_image();" required /></span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error1").hide();'>Remove</a>
			  </div>
			</div>
				
			
			
			
			<p class="create"> 
			<h2> Title Headline: </h2>
			<textarea class="ckeditor" name="title" id="title" required></textarea>	
			<br>
			<br>
			</p>
			
			<div class="templateID2">
				<h2> Featured Image:</h2> <div id="error21"> <h2> <font color="red">Error, Wrong Image Format!</font> </h2> </div>
				<div class="fileupload fileupload-new" data-provides="fileupload">
				  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
				  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
				  <div>
					<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input class="templateID2" type="file" name="fimage" id="fimage" required /></span>
					<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error21").hide();'>Remove</a>
				  </div>
				 </div>
			 </div></br>
			
			<h2> First Body Image:</h2> <div id="error2"> <h2> <font color="red">Error, Wrong Image Format!</font> </h2> </div>
			<div class="fileupload fileupload-new" data-provides="fileupload">
			  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
			  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
			  <div>
				<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="simage" id="simage" onchange="check_image();" required /></span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error2").hide();'>Remove</a>
			  </div></div></br>
			
			<div class="templateID2">
				<h2> Second Body Image:</h2> <div id="error22"> <h2> <font color="red">Error, Wrong Image Format!</font> </h2> </div>
				<div class="fileupload fileupload-new" data-provides="fileupload">
				  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
				  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
				  <div>
					<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input class="templateID2" type="file" name="simage2" id="simage2"  required /></span>
					<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error22").hide();'>Remove</a>
				  </div>
				</div>
			</div></br>
			
			<div class="templateID2">
			<h2> Magazine Image:</h2> <div id="error23"> <h2> <font color="red">Error, Wrong Image Format!</font> </h2> </div>
			<div class="fileupload fileupload-new" data-provides="fileupload">
			  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
			  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
			  <div>
				<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input class="templateID2" type="file" name="mimage" id="mimage" " required /></span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error23").hide();'>Remove</a>
			  </div></div></div></br>
			
			<div class="templateID2">
			<h2> Celebrity Image:</h2> <div id="error24"> <h2> <font color="red">Error, Wrong Image Format!</font> </h2> </div>
			<div class="fileupload fileupload-new" data-provides="fileupload">
			  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
			  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
			  <div>
				<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input class="templateID2" type="file" name="cimage" id="cimage" ;" required /></span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error24").hide();'>Remove</a>
			  </div></div></div></br>
			
			<p class="create"> 
			<h2> Offer 1 </h2>
			<input type="text" name="anchor1" required />
			<br>
			<br>
			</p>
			
			<p class="create"> 
			<h2> Offer 1 Link</h2><div id="errorURL1"> <h2> <font color="red">Error, Please Insert a URL starting with http:// </font> </h2> </div>
			<input type="text" name="link1" id="link1" required onblur="ValidURL();" />
			<br>
			<br>
			</p>
			
			<p class="create"> 
			<h2> Offer 1 Exit Link</h2><div id="errorURL11"> <h2> <font color="red">Error, Please Insert a URL starting with http:// </font> </h2> </div>
			<input type="text" name="exit1" id="exit1" required onblur="ValidURL();" />
			<br>
			<br>
			</p>
			
			
			<p class="create"> 
			<h2> Offer 2</h2>
			<input type="text" name="anchor2" required />
			<br>
			<br>
			</p>
			
			<p class="create">
			<h2> Offer 2 Link</h2><div id="errorURL2"> <h2> <font color="red">Error, Please Insert a URL starting with http:// </font> </h2> </div>
			<input type="text" name="link2" id="link2" required onblur="ValidURL();" />
			<br>
			<br>
			</p>
			
			<p class="create"> 
			<h2> Offer 2 Exit Link</h2><div id="errorURL21"> <h2> <font color="red">Error, Please Insert a URL starting with http:// </font> </h2> </div>
			<input type="text" name="exit2" id="exit2" required onblur="ValidURL();" />
			<br>
			<br>
			</p>
			
			<h2> Offer Image 1:</h2> <div id="error3"> <h2> <font color="red">Error, Wrong Image Format!</font> </h2> </div>
			<div class="fileupload fileupload-new" data-provides="fileupload">
			  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
			  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
			  <div>
				<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="offerimg1" id="offerimg1" onchange="check_image();"  /></span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error3").hide();'>Remove</a>
			  </div>
			</div>
			<br/>
			<br/>
			
			<h2> Offer Image 2:</h2> <div id="error4"> <h2> <font color="red">Error, Wrong Image Format!</font> </h2> </div>
			<div class="fileupload fileupload-new" data-provides="fileupload">
			  <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
			  <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: auto; max-height: auto; line-height: auto;"></div>
			  <div>
				<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" name="offerimg2" id="offerimg2" onchange="check_image();" /></span>
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload" onclick='$("#error4").hide();'>Remove</a>
			  </div>
			</div>
			<br/>
			<br/>
			
			<div class="templateID2">
			<p class="create"> 
			<h2> Revision Number</h2>
			<input class="templateID2" type="text" name="revision" required />
			<br>
			<br>
			</p></div>
			
			<h2>Google Analytics Code</h2> <div id="errorTextArea"><h2><font color="red>Error, This Field is Required"</font></h2></div>
			<textarea name="gacode" id="gacode" rows="15" cols="50" required></textarea>
			
			<br/>
			<br/>
			<br/>
			
			<input type="hidden" name="templateID" id="templateID" value="<?php echo $_POST['templateID']; ?>" >
			
			<p class="create">
			<input type="submit" name="create" id="create" value="create" >
			</p>
		</form>
		
		
		
	

</section>
</body>

</html>