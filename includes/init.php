<?php

include_once('includes/dbconfig.php');
include_once('classes/user.php');
include_once('classes/skinTemplate.php');

$conn = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);

$cwd = realpath(__DIR__ . '/..'); 

$user = new user($conn, $cwd);

$skinTemplate = new skinTemplate($conn, $cwd);

//$user->auth_login('admin', 'admin');

