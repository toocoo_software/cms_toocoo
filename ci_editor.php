<?php
include('includes/init.php');
include_once('includes/utils.php');
session_start();

//check login status
if ($user->checkLoginStatus() == FALSE) {
	header("Location: login.php");
}

	
//get campaing name from URL
$campaign = $_GET['campaign'];

//need to sanitize this further because
//it is being put into a shell_exec command later on
$campaign = stripslashes($campaign); 
$campaign = str_replace('/','',$campaign);
$campaign = htmlentities($campaign);


//return an array of all the values of the database
//This is used inside edit.php
$result = $skinTemplate->display_value($campaign);

if (isset($_POST['edit'])){


	$campaign = $_GET['campaign'];
	
	$id2=0;
	$id3=0;
	$id4=0;
	$id5=0;
	$id6=0;
	$id7=0;
	$id8=0;
	$id9=0;
	$id10=0;
	$id11=0;
	$id12=0;
	$id13=0;
	$id14=0;
	$id15=0;
	$id16=0;
	$id17=0;
	$id18=0;
	$id19=0;
	$id20=0;

	$templateID = $_POST['templateID'];
	
	
	if ($templateID == 1){
		//set your folder path
		$path = $campaign . '/index1_files/' ; 
		$path2 = '/index1_files/' ; 

	}

	if ($templateID ==2){
		//set your folder path
		$path = $campaign . '/images/' ; 
		$path2 = '/images/' ; 
		
		//variables exclusive only to Template 2

		//featured image
		$fimage = edit_image('fimage' , $path);
		
		if(!empty($fimage)){
			$skinTemplate->edit_page($campaign, "fimage", $fimage, $templateID);
			$id16 = 1;
		}

		//second body image
		$simage2 = edit_image('simage2' , $path);
		
		if(!empty($simage2)){
			$skinTemplate->edit_page($campaign, "simage2", $simage2, $templateID);
			$id17 = 1;
		}

		//magazine image
		$mimage = edit_image('mimage' , $path);

		if(!empty($mimage)){
			$skinTemplate->edit_page($campaign, "mimage", $mimage, $templateID);
			$id18 = 1;
		}
		
		//celebrity image
		$cimage = edit_image('cimage' , $path);

		if(!empty($cimage)){
			$skinTemplate->edit_page($campaign, "cimage", $cimage, $templateID);
			$id19 = 1;
		}
		
		//revision number
		$revision = $_POST['revision'];
		
		if ($revision != $result[20]){
			$skinTemplate->edit_page($campaign, "revision" , $revision, $templateID);
			$id20 = 1;
		}
		
	}
	

	//header Image
	$header = edit_image('header', $path);
	
	if(!empty($header)){
		$skinTemplate->edit_page($campaign, "header", $header, $templateID);
		$id2 = 1;
	}
	
	
	//Headline 
	$title = $_POST['title'];
	if ($title != $result[3]){
		$skinTemplate->edit_page($campaign, "title" , $title, $templateID);
		$id3 = 1;
	}
	
	//Side Image
	$simage = edit_image('simage' , $path);
	
	if(!empty($simage)){
		$skinTemplate->edit_page($campaign, "simage", $simage, $templateID);
		$id4 = 1;
	}

	//offer 1 anchor text 
	$anchor1 = $_POST['anchor1'];
	if ($anchor1 != $result[6]){
		$skinTemplate->edit_page($campaign, "anchor1" , $anchor1, $templateID);
		$id6 = 1;
	}

	//offer 1 affiliate link
	$link1 = $_POST['link1'];
	if ($link1 != $result[7]){
		$skinTemplate->edit_page($campaign, "link1" , $link1, $templateID);
		$id7 = 1;
	}

	//offer 2 anchor text 
	$anchor2 = $_POST['anchor2'];
	if ($anchor2 != $result[8]){
		$skinTemplate->edit_page($campaign, "anchor2" , $anchor2, $templateID);
		$id8 = 1;
	}

	//offer 2 affiliate link
	$link2 = $_POST['link2'];
	if ($link2 != $result[9]){
		$skinTemplate->edit_page($campaign, "link2" , $link2, $templateID);
		$id9 = 1;
	}

	//offer 1 Image
	$offerimg1 = edit_image('offerimg1' , $path);
	
	if(!empty($offerimg1)){
		$skinTemplate->edit_page($campaign, "offerimg1", $offerimg1, $templateID);
		$id10 = 1;
	}

	//offer 2 Image
	$offerimg2 = edit_image('offerimg2' , $path);
	
	if(!empty($offerimg2)){
		$skinTemplate->edit_page($campaign, "offerimg2", $offerimg2, $templateID);
		$id11 = 1;
	}
	
	$gacode = $_POST['gacode'];
	if ($gacode != $result[12]){
		$skinTemplate->edit_page($campaign, "gacode" , $gacode, $templateID);
	}
	
	//exit 1 affiliate link
	$exit1 = $_POST['exit1'];
	if ($exit1 != $result[13]){
		$skinTemplate->edit_page($campaign, "exit1" , $exit1, $templateID);
		$id13 = 1;
	}
	
	//exit2 affiliate link
	$exit2 = $_POST['exit2'];
	if ($exit2 != $result[14]){
		$skinTemplate->edit_page($campaign, "exit2" , $exit2, $templateID);
		$id14 = 1;
	}
	
	
		
	//this contains all campaigns that can be selected
	$result = $skinTemplate->listCampaigns($campaign);
	
	//this 'for' loop only returns the campaigns that have been checked
	$k=0;
	for ($i=0; $i<count($result);$i++){
		if($_POST[$result[$i]] == 1){
			$campaigns[$k]=$result[$i];
			$k++;
		}
	}
	
	//Find the elements that have changed
	
	if($id2==1){
	$changed = ',header';
	copy_image($header, $campaign, $campaigns, $path2);
	}
	
	if($id3==1){
	$changed = $changed . ',title';
	}
	
	if($id4==1){
	$changed = $changed . ',simage';
	copy_image($simage, $campaign, $campaigns, $path2);
	}
	
	if($id6==1){
	$changed = $changed . ',anchor1';
	}
	
	if($id7==1){
	$changed = $changed . ',link1';
	}
	
	if($id8==1){
	$changed = $changed . ',anchor2';
	}
	
	if($id9==1){
	$changed = $changed . ',link2';
	}
	
	if($id10==1){
	$changed = $changed . ',offerimg1';
	copy_image($offerimg1, $campaign, $campaigns, $path2);
	}
	
	if($id11==1){
	$changed = $changed . ',offerimg2';
	copy_image($offerimg2, $campaign, $campaigns, $path2);
	}
	
	if($id13==1){
	$changed = $changed . ',exit1';
	}
	
	if($id14==1){
	$changed = $changed . ',exit2';
	}
	
	if($id16==1){
	$changed = $changed . ',fimage';
	copy_image($fimage, $campaign, $campaigns, $path2);
	}
	
	if($id17==1){
	$changed = $changed . ',simage2';
	copy_image($simage2, $campaign, $campaigns, $path2);
	}
	
	if($id18==1){
	$changed = $changed . ',mimage';
	copy_image($mimage, $campaign, $campaigns, $path2);
	}
	
	if($id19==1){
	$changed = $changed . ',cimage';
	copy_image($cimage, $campaign, $campaigns, $path2);
	}
	
	if($id20==1){
	$changed = $changed . ',revision';
	}
	
	
	//remove first comma (,) from string
	$changed = substr($changed, 1);

	//put comma separated string array
	$elements = explode(',', $changed);
	
	//values are the same as the elements
	//except the are appended with $ at the beginning
	for($j=0;$j<count($elements);$j++){
		$values[$j] = ${$elements[$j]};
	}
	
	if(!empty($campaigns) || !empty($elements) || !empty($values)){
	
	$skinTemplate->proliferate_pages($campaigns, $elements, $values, $templateID);
	}
	
	header("Location: ".$campaign."");
}



