<?php
include('includes/init.php');

//check login status
if ($user->checkLoginStatus() == FALSE) {
	header("Location: login.php");
}
	
//Delete entry in DB
$folder = $_REQUEST['campaign'];

//Sanitize input and make sure it's a number

$folder = stripslashes($folder); 

$folder = str_replace('/','',$folder);

$folder = htmlentities($folder);

$campaigns = $skinTemplate->listCampaigns("0");	

if (in_array($folder, $campaigns) == TRUE) 
	{
		//delete database entry
		$user->delete_page($folder);
		//Delete the folder
		$user->rrmdir($folder);
		header("Location: admin.php");
	}

else{
	header("Location: 404.php");
	exit;
}



