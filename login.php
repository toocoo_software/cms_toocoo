<?php
include("includes/init.php");
session_start();

//If user is logged in, forward them to admin dashboard, else stay here
if ($user->checkLoginStatus()== TRUE){
	header("Location: admin.php");
}



//If they press the submit button 

if (isset($_POST['submit'])) {
	$user->prep_login($_POST['username'] , $_POST['password']);
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Toocoo CMS</title>

<section class="container">
	<div class="login">
		<h1>Login to CMS</h1>
		<form method="post" action="login.php">

			<p><label> Username: <input type="text" name="username" required /></label></p>
			<p><label> Password: <input type="password" name="password" required /></label></p>
			
			<p class="submit"><input type="submit" name="submit" value="Login"></p>
		</form>
	</div>
</section>

<body>


</body>

</html>