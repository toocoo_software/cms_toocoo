<?php
include('includes/init.php');
session_start();

//check login status
if ($user->checkLoginStatus() == FALSE) {
	header("Location: login.php");
}

//If user presses logout button
if (isset($_POST['logout'])) {
	$user->logout();
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="css/style.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.9.1.js"></script>
  <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script>
  $(function() {
    $( "#dialog-confirm" ).dialog({
      resizable: false,
      height:140,
      modal: true,
      buttons: {
        "Delete all items": function() {
          $( this ).dialog( "close" );
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });
  });
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.templates').hide(); 
	});
	
			
	function showTemplates(){
		$('#createbutton').hide();
		$('.templates').show();	
	}
</script>
</head>
<body>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Toocoo CMS</title>

<section class="container2">
	<div class="admin">
		<h1>Admin Dashboard</h1>
		
		<!-- Logout button -->
		<form method="post" action="admin.php">
			<p class="submit"><input type="submit" name="logout" value="Logout"></p>
		</form>
		
		<!-- <div id="dialog-confirm" title="Empty the recycle bin?">
		
		<!-- Create page link -->
		<p class="create">
			<div class="createbutton2">
				<input type="image" id="createbutton" src="img/create.png" onClick="showTemplates();">
			</div>
		
		<!-- Create page form Template 1-->
		<div class="templates">
			<p class="create">
				<form target="_blank" method="post" action="create.php">
					<input type="image" src="img/template1.png" value="submit" >
					<input type="hidden" id="templateID" name="templateID" value=1>
				</form>
				</br>
				</br>
		</div>
		
		<!-- Create page form Template 2-->
		<div class="templates">
			<p class="create">
				<form target="_blank" method="post" action="create.php">
					<input type="image" src="img/template2.png" value="submit" >
					<input type="hidden" id="templateID" name="templateID" value=2>
				</form>
				</br>
				</br>
		</div>
		
		<?php 
			//return one array that contains the rowCount (Num of campaigns) in each Template.
			//return second 2D array $result that contains the Campaign names in each Template.
			$array = $user->displayPagesT1();
			$rowCount = $array[0];
			$result = $array[1];
			$templateCount = 2;
			
			
			for ($k=1;$k<$templateCount+1;$k++){

				$domain = $_SERVER['HTTP_HOST'];
				echo '</br></br>';
				echo '<h2>Template '.$k.'</h2>';
				for ($j=0; $j < $rowCount[$k]; $j++){
					echo '<p class="follow-up">';
					echo '<a href="http://'.$domain.'/cms/'.$result[$k][$j].'" target="_blank"><h3 class="campaign-name"><font face="verdana" size="20">'.$result[$k][$j].'</font></h3></a>';
					//echo '<a href="delete.php?campaign='.$result[$k][$j].'" ><img class="campaign-delete" align="right" src="img/delete.png" height="40" width="40"></a>';
					echo '<form method="post" action="delete.php?campaign='.$result[$k][$j].'"><input type="image" class="campaign-delete" align="right" src="img/delete.png" height="40" width="40"" value="submit" onclick="return confirm(\'Are You Sure You Want to Delete This Campaign?\');"></form>';
					echo '<a href="edit.php?campaign='.$result[$k][$j].'&templateID='.$k.'" target="_blank"><img class="campaign-delete" align="right" src="img/edit.png" height="40" width="40"></a>';
					echo '</p>';
					echo '</br> </br> </br>';
				}
			}
		
			
			
		?>
		
		</p>
	</div>
</section>
</body>

</html>